import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  deleteItem(key: string) {
    this.authService.user.subscribe(user =>{
        this.db.list('/users/'+user.uid+'/items').remove(key);
      })
  }
 
  addTodo(text1: string,text2:string,text3:string) {
    this.authService.user.subscribe(
     _user => {
        this.db.list('/users/'+text3+'/items').push({'nameOfpayer': text1,'sum': text2});
      }
    )
  }
 
  update(key,text1,text2){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/items').update(key,{'nameOfpayer': text1,'sum': text2})
    })
  }
  
updateDone(key:string, _text:string, stock:boolean)
{
  this.authService.user.subscribe(user =>{
    this.db.list('/users/'+user.uid+'/items').update(key,{'stock':stock});
 })

}

  constructor(private authService:AuthService,
    private db:AngularFireDatabase,
  ) { }

}
