import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database'
import {AuthService} from '../auth.service';
import {ItemsService} from '../items.service';

@Component({
  selector: 'payments',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items=[];
  text1:string;
  text2:string;
  text3:string;
  user=[];
  details=[];
  address;
send=false;

  addTodo() {
    this.ItemsService.addTodo(this.text1,this.text2,this.text3);
    this.text1 = '';
    this.text2 = '';
    this.text3 = '';
    this.send=true;
  }
 
  constructor(private db:AngularFireDatabase, 
    public authService:AuthService,
  private ItemsService:ItemsService) { }

  ngOnInit() {
   this.authService.user.subscribe(
      user => {
        if(!user) return;
//console.log(user);
        this.db.list('/users/'+user.uid+'/details').snapshotChanges().subscribe(
          details => {
            this.details = [];
            details.forEach(
                  detail => {
                    let y = detail.payload.toJSON();
                    y["$$key"] = detail.key;
                    this.details.push(y);            
                     this.address = this.user[0].address;         
              }
            )
            
           
          }
        )
      
      
    this.db.list('/users/'+user.uid+'/items').snapshotChanges().subscribe(
      items => {
        this.items = [];
        items.forEach(
            item=>{
              let y =item.payload.toJSON();
              y["$key"] = item.key;
              this.items.push(y);
            }
          )
 
      }
    )

 
  }
    )
   }  }
