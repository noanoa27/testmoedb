import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { AuthService } from '../auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  error='';
 code='';
  message='';
 

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);


  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  login() {
    this.authService.login(this.email, this.password)
    .then(value => {
      this.router.navigate(['/']);
      console.log(value);
    })
    .catch(err => {
       this.code=err.code;
      this.message=err.message;
      console.log(err);
    })
  }

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

}
