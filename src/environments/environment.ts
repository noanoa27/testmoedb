// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAcl9FHl-jkVE2YVUBtkG9rus7B3tTNBV4",
    authDomain: "testb-ca943.firebaseapp.com",
    databaseURL: "https://testb-ca943.firebaseio.com",
    projectId: "testb-ca943",
    storageBucket: "testb-ca943.appspot.com",
    messagingSenderId: "697868678997"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
